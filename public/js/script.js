$(document).ready(function(){
	$('#book-list select').change(function(event){

		bookID = this.value;

		$.ajax({
  		type: 'POST',
  		url: '/get-book-info',
  		data: {"bookID":bookID, "_token": $('meta[name="csrf-token"]').attr('content')},
  		success: writeInfoBook
		});

	});

	$('#author-list select').change(function(event){
		authorID = this.value;
		$.ajax({
  		type: 'POST',
  		url: '/get-author-info',
  		data: {"authorID":authorID, "_token": $('meta[name="csrf-token"]').attr('content')},
  		success: writeInfoAuthor
		});

	});
})

function writeInfoBook(resp){
	let authors = resp.authors;
	let book = resp.book;
	let html = '<h4>'+book.title+'</h4>'+'<p>цена '+book.price+'р<br>'+book.description+'</p>'+'<h4>Авторы этой книги</h4><p>';
	for(i = 0; i < authors.length; i++){
		let author = authors[i];
		html += author.surname+' '+author.name+' '+author.patronymic+'<br>';
	}
	html += '</p>';
	$('#info').html(html);
}

function writeInfoAuthor(resp){
	console.log(resp.books);
	let author = resp.author;
	let books = resp.books;
	let costBooks = resp.costBooks;
	let html = '<h3>'+author.surname+' '+author.name+' '+author.patronymic+'</h3>';
	let book;
	html += '<h4>Об авторе </h4><p>';
	html += author.description+'</p><h3>Книги автора</h3><p>';
	for(i = 0; i < books.length; i++){
		book = books[i];
		html += book.title+' '+book.price+'р<br>';
	}
	html +='стоимость всех книг '+costBooks+'р';
	html += '</p>';
	$('#info').html(html);
}