<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->defineAs(App\Book::class, 'book', function (Faker $faker) {
    return [
        'title' => $faker->sentence(2, true),
        'price' => rand(100,1000),
        'description' => $faker->text()
    ];
});

$factory->defineAs(App\Author::class, 'author', function (Faker $faker) {
	$faker = \Faker\Factory::create('ru_RU');
	$fullName = explode(' ', $faker->name());

    return [
        'name' => $fullName[0],
        'surname' => $fullName[1],
        'patronymic' => $fullName[2],
        'description' => $faker->text()
    ];
});