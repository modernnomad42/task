<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // в этом случае у одной книги будет только один автор, а у одного автора только одна книга.
        // factory(App\Book::class, 'book', 20)->create()->each(function($book){
        // 	$book->authors()->save(factory(App\Author::class, 'author')->make());
        // });

        $bookQty = 20;
        $authorQty = 10;
        $relationQty = 15;

        factory(App\Book::class, 'book', $bookQty)->create();
        factory(App\Author::class, 'author', $authorQty)->create();

        $authorsIDs = DB::table('authors')->pluck('id');
        $booksIDs = DB::table('books')->pluck('id');

        for($i = 0; $i < $relationQty; $i++){
        	$randAuthorID = $authorsIDs[rand(0, (count($authorsIDs) -1 ))];
            $randBookID = $booksIDs[rand(0, (count($booksIDs)-1))];
            $isExists = DB::table('author_book')->where([
  							['author_id', '=', $randAuthorID],
  							['book_id', '=', $randBookID],
						])->get()->count();
            if($isExists != 0){
            	continue;
            }
        	DB::table('author_book')->insert(
  				[
  			 		'author_id' => $randAuthorID,
  			 		'book_id' => $randBookID
  				]
			);
        }
    }
}
