<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;

class AuthorController extends Controller
{
    public function getAuthorInfo(Request $request){
		$author = Author::find($request->authorID);
		$books = $author->books;
		$costBooks = $author->calculateCostBooks();

		return response()->json(array(
                    'success' => true,
                    'author' => $author,
                    'books' => $books,
                    'costBooks' => $costBooks
                ),
			  200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        JSON_UNESCAPED_UNICODE);
	}
}