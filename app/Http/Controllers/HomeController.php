<?php

namespace App\Http\Controllers;

use App\Book;
use App\Author;

class HomeController extends Controller
{
	public function index(){
		$authors = Author::all();
		$booksWithAuthors = Book::getBooksWithAuthors();
		$booksWithouthAuthors = Book::getBooksWithoutAuthors();
		return view('home.index', [
			'booksWithAuthors' => $booksWithAuthors,
			'booksWithouthAuthors' => $booksWithouthAuthors,
			'authors' => $authors
		]);
	}
}