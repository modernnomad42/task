<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    public function getBookInfo(Request $request){
		$book = Book::find($request->bookID);
		$authors = $book->authors;
		return response()->json(array(
					'book' => $book,
                    'authors' => $authors
                ),
			  200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        JSON_UNESCAPED_UNICODE);
	}
}
