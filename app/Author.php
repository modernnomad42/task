<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public $timestamps = false;

    public function books(){
    	return $this->belongsToMany('App\Book');
  	}

  	public function calculateCostBooks(){
  		$books = $this->books()->get();
  		$cost = 0;
  		foreach($books as $book){
  			$cost += $book->price;
  		}
  		return $cost;
  	}
}
