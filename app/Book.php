<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Book extends Model
{
	public $timestamps = false;

    public function authors(){
    	return $this->belongsToMany('App\Author');
  	}

  	public static function getBooksWithAuthors(){
  		return DB::select('SELECT * FROM books WHERE id IN (select distinct book_id from author_book)');
  	}

  	public static function getBooksWithoutAuthors(){
  		return DB::select('SELECT * FROM books WHERE id NOT IN (select distinct book_id from author_book)');
	}
}