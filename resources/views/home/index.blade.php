@extends('layouts.main')
@section('title', 'Тестовое задание')
@section('content')
<div class="container content">
     <div id="info">
     </div>
	<form id="book-list" class="section">
		<h2>Книги чьи авторы известны</h2>
		{{ csrf_field() }}
		<select>
               <option selected disabled>Выберите книгу</option>
     	<?php foreach($booksWithAuthors as $book):?>
     		<option value="<?= $book->id?>"><?= $book->title?></option>
     	<?php endforeach;?>
     	</select>
 	</form>
 	<form id="author-list" class="section">
		<h2>Авторы</h2>
		{{ csrf_field() }}
		<select>
               <option selected disabled>Выберите автора</option>
     	<?php foreach($authors as $author):?>
     		<option value="<?= $author->id?>"><?= $author->surname.' '.$author->name.' '.$author->patronymic?></option>
     	<?php endforeach;?>
     	</select>
 	</form>
 	<div id="book-without-author" class="section">
 		<h2>Книги без авторов</h2>
 		<ul>
             <?php foreach($booksWithouthAuthors as $book):?>
 			<li><?= $book->title?><span class="price"><?= $book->price?>р</span></li>
             <?php endforeach;?>
 		</ul>
 	</div>

 </div>
@endsection