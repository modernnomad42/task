
INSERT INTO `authors` (`id`, `name`, `surname`, `patronymic`, `description`) VALUES
(1, 'Влада', 'Маркова', 'Анатолиевна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(2, 'Роман', 'Логиновский', 'Гаврилович', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(3, 'Родислав', 'Буклин', 'Радионович', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(4, 'Светлана', 'Пелевина', 'Игоревна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(5, 'Софья', 'Сподарева', 'Владленовна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(6, 'Иван', 'Карданов', 'Эдуардович', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(7, 'Марианна', 'Грибова', 'Иосифовна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(8, 'Берта', 'Пережогина', 'Анатолиевна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(9, 'Василиса', 'Другакова', 'Игоревна', 'Об авторе этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени');

INSERT INTO `books` (`id`, `title`, `price`, `description`) VALUES
(1, 'Персональный закон', 120, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(2, 'Ветер Одиссеи', 110, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(3, 'Преодолевая километры', 130, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(4, 'Солнце которое не светит', 134, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(5, 'Вечное путешествие', 194, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(6, 'Тайна клана лжецов', 1000, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(7, 'Копилка неудачника', 1000, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(8, 'Мы невозможны', 999, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(9, 'Ледяной', 760, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(10, 'Биография неизвестного', 666, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(11, 'Пульсация', 400, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(12, 'Сказочное', 300, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(13, 'Одиночество', 300, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(14, 'Миру мир', 300, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(15, 'в пустоту', 290, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени'),
(16, 'Чертёнок хочет поиграть', 1800, 'Описание этой книги.богатый опыт постоянный количественный рост и сфера нашей активности требуют от нас анализа соответствующий условий активизации. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в значительной степени');


INSERT INTO `author_book` (`author_id`, `book_id`) VALUES
(1, 1),
(1, 2),
(1, 4),
(2, 4),
(3, 4),
(3, 11),
(4, 4),
(4, 5),
(4, 12),
(5, 5),
(6, 7),
(6, 9),
(7, 2),
(7, 7),
(8, 2),
(9, 10);